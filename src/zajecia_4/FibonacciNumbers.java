package zajecia_4;

import java.util.Scanner;

public class FibonacciNumbers {

    public static void main(String[] args) {
        System.out.println("Application obliczajacy n-ty wyraz ciagu Fibonacciego");
        System.out.println("Podaj n");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int result = MathHelper.fibonacci(n);
        System.out.println(n + " wyraz ciagu Fibonacciego = " + result);

    }
}
