package zajecia_4;

import java.util.Scanner;

public class TriangleArea {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Application sprawdzi czy z trzech odcinkow mozna zbudowac trojkat oraz obliczy jego pole");
        System.out.println("Podaj dlugosci trzech odcinkow");
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();

        if (a + b > c && a + c > b && b + c > a) {
            double area = MathHelper.areaOfTriangle(a, b, c);
            System.out.println("Mozna zbudowac trojkat z tych odcinkow, a jego pole = " + area);
        } else {
            System.out.println("Nie mozna zbudowac trojkata z tych odcinkow");
        }
    }

}
