package zajecia_4;

import java.util.Random;
import java.util.Scanner;

public class ArrayRealNumbers4to15 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Podaj ilosc liczb do wylosowania");
        int n = scanner.nextInt();
        double[] tab = new double[n];

        for (int i = 0; i < n; i++) {
            tab[i] = random.nextDouble() * 100;
        }

        System.out.println("Elementy tablicy z zakresu [4;15): ");
        for (int i = 0; i < n; i++) {
            if (tab[i] >= 4 && tab[i] < 15) {
                System.out.println("Indeks: " + i + " wartosc: " + tab[i]);
            }
        }


    }
}
