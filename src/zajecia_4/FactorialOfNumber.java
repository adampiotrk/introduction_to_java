package zajecia_4;

import java.util.Scanner;

public class FactorialOfNumber {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna, dla ktorej ma byc policzona silnia");
        int a = sc.nextInt();
        int result = MathHelper.factorial(a);
        System.out.println(a + "! = " + result);
    }

}
