package zajecia_4;

import java.util.Scanner;

public class ArrayNaturalNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb calkowitych, n <1:30>");
        int n = scanner.nextInt();
        int[] tab = new int[n];

        if (n >=1 && n <= 30) {
            System.out.println("Elementy tablicy: ");
            for (int i = 0; i < tab.length; i++) {
                tab[i] = i * i;
                System.out.print(tab[i] + ", ");
            }
        } else {
            System.out.println("Nieprawidlowa wartosc n");
        }
    }

}
