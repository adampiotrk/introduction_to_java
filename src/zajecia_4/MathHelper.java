package zajecia_4;

// klasa pomocnicza z obliczeniami matematycznymi
public class MathHelper {
    /**
     * Klasa narzedziowa - nie pozwalamy na tworzenie obiektow typu MathHelper
     */
    private MathHelper () {};

    /**
     * Metoda obliczajaca pole trojkata z dlugosci 3 odcinkow
     */
    public static double areaOfTriangle(double a, double b, double c) {
        double p = 0.5 * (a + b + c);
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    /**
     * Metoda obliczajaca silnie z n, n!
     */
    public static int factorial(int a) {
        int result = 1;

        for (int i = 1; i <= a; i++) {
            result *= i;
        }
        return result;
    }

    /**
     * metoda oblczajaca n-ty wyraz ciagu fibonacciego
     */
    public static int fibonacci(int n) {
        int result0 = 0;
        int result1 = 1;
        int result = 0;
        if (n == 0) {
            result = result0;
        } else if (n == 1) {
            result = result1;
        } else {
            for (int i = 2; i <= n; i++) {
                result = result0 + result1;
                result0 = result1;
                result1 = result;
            }
        }
        return result;
    }

}
