package zajecia_4;

import java.util.Random;
import java.util.Scanner;

public class MaxAndMinInArray {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        System.out.println("Podaj ilosc elementow do wylosowania z zakresu 0 do 99");
        int n = sc.nextInt();
        int[] numbers = new int[n];

        System.out.print("Wylosowano liczby: ");
        for (int i = 0; i < n; i++) {
            numbers[i] = r.nextInt(100);
            System.out.print(numbers[i] + " ");
        }

        int max = numbers[0];
        for (int i = 0; i < n; i++) {
            if (max < numbers[i]) {
                max = numbers[i];
            }
        }

        int min = numbers[0];
        for (int i = 0; i < n; i++) {
            if (min > numbers[i]) {
                min = numbers[i];
            }
        }

        System.out.println("\nNajwieksza liczba w zbiorze to: " + max);
        System.out.println("Najmniejsza liczba w zbiorze to: " + min);


        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += numbers[i];
        }
        int average = sum / n;
        System.out.println("Srednia wazona elementow tablicy = " + average);
    }
}
