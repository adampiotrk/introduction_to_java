package zajecia_4;

public class ArrayIntro {

    public static void main(String[] args) {

        int[] dane = new int[10];
        for (int i = 0; i < dane.length; i++) {
            dane[i] = i * i;
        }

        for (int i = 0; i < dane.length; i++) {
            System.out.println("Indeks: " + i + " wartosc: " + dane[i]);
        }

        // policzyc sume elementow tablicy
        int suma = 0;
        for (int i = 0; i < dane.length; i++) {
            suma += dane[i];
        }
        System.out.println("Suma elementow tablicy = " + suma);

        //policzyc srednia elementow tablicy
        System.out.println("Sreednia elementow tablicy = " + ((double) suma / dane.length));
    }
}
