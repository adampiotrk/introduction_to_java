package zajecia_4;

import java.util.Random;
import java.util.Scanner;

public class MultiplicationGame {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random r = new Random();
        int a = r.nextInt(10);
        int b = r.nextInt(10);
        int x;
        System.out.println("Podaj wynik mnozenia: " + a + " * " + b + " = ?");

        do {
            x = input.nextInt();
            if (x == a * b) {
                System.out.println("Gratulacje! Poprawna odpowiedź");
            } else {
                System.out.println("Niestety :( Spróbuj ponownie");
            }
        } while (x != a * b);

    }
}
