package zajecia_3;

import java.util.Scanner;

public class PrimeChecker {
    public static boolean isPrime(int number) {
        boolean result = true;
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                result = false;
                break;
            }
        }
        return result;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Wprowadz liczbe");
        int a = sc.nextInt();

        boolean wynik = isPrime(a);
        System.out.println("Czy jest to liczba pierwsza: " + wynik);

    }
}
