package zajecia_3;


// program wyswieltajacy wszystie wielkie litery alfabetu od A do Z i od Z do A
public class AlphabetAZandZA {

    public static void main(String[] args) {

        System.out.println("Od A do Z");
        for (int i = 65; i <= 90; i++) { //zamiast int i = 65 można wpisać char i = 'A', nie trzeba sie ograniczac do liczb
            System.out.println((char) i);
        }

        System.out.println("Od Z do A");
        for (int i = 90; i >= 65; i--) {
            System.out.println((char) i);
        }

        char c = 'A';
        while (c <= 'Z') {
            System.out.println("Znak: " + c + " pozycja: " + (short) c);
            c++;
        }
    }
}
