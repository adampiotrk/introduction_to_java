package zajecia_3;


// od 0 -100 wypisac nieparzyste z uzyciem petli do while
public class PetlaDoWhile {

    public static void main(String[] args) {

        int licznik = 0;
        do {
            if (licznik % 2 != 0) {
                System.out.println(licznik);
            }
            licznik++;
        } while (licznik < 100);


    }
}
