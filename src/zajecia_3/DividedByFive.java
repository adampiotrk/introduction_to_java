package zajecia_3;


// wypisac liczby podzielne przez 5 od 0 do 500 rosnaco, a potem od tylu
public class DividedByFive {

    public static void main(String[] args) {

        System.out.println("Liczby od 0 do 500 podzielne przez 5 rosnaco:");
        for (int i = 5; i <= 500; i += 5) {
            System.out.print(i + ", ");
        }

        System.out.println("\nLiczby od 0 do 500 podzielne przez 5 malejaco:");
        for (int i = 500; i >= 5; i -= 5) {
            System.out.print(i + ", ");
        }
    }
}
