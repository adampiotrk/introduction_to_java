package zajecia_3;

import java.util.Scanner;

public class ParzysteZakres {

    public static void main(String[] args) {
        int bottom;
        int top;

        System.out.println("Wskaz przedzial, z ktorego maja byc wyswietlane liczby parzyste");
        System.out.println("Dol zakresu");
        Scanner sc = new Scanner(System.in);
        bottom = sc.nextInt();
        System.out.println("Gora zakresu");
        top = sc.nextInt();

        System.out.println("\n Liczby parzyste w tym zakresie to:");
        while (bottom <= top) {
            if (bottom % 2 == 0) {
                System.out.print(bottom + ", ");
            }
            bottom++;
        }
    }
}

