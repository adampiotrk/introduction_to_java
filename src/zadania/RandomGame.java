package zadania;

import java.util.Random;
import java.util.Scanner;

public class RandomGame {

    public static void main(String[] args) {
        System.out.println("Spróbuj zgadnąć liczbę z zakresu od 0 do 9");
        Random r = new Random();
        int number = r.nextInt(10);
        int x;
        int i = 1;

        do {
            System.out.println("Wprowadź liczbę");
            Scanner input = new Scanner(System.in);
            x = input.nextInt();
            if (x == number) {
                System.out.println("Gratulacje! Zgadłeś przy " + i + " próbie :)");
            } else {
                i++;
                System.out.println("Niestety :( Spróbuj ponownie");
            }
        } while (x != number);

    }
}
