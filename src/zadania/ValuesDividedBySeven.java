package zadania;

public class ValuesDividedBySeven {

    public static void main(String[] args) {

        System.out.println("Application wypisujacy liczby od 0 do 500 podzielne przez 7");

        int a = 0;

        while (a <= 500) {
            if (a % 7 == 0) {
                System.out.println(a);
                a++;
            } else {
                a++;
            }
        }
    }
}
