package zadania;

import java.util.Scanner;

// program zlicza ilosc znakow w zdaniu
// a - z bialymi znakami (spacja, tabulator)
// b - bez bialych znakow

public class StringLengthCount {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String zdanie;

        System.out.println("Application do zliczania ilosci znakow w zdaniach");
        System.out.println("Wpisz zdanie do sprawdzenia");
        zdanie = sc.nextLine();

        // wyswietlic menu
        System.out.println("\n1. Uwzglednic biale znaki. \n2. Pominac biale znaki. \n3. Wyjscie");
        int choice = sc.nextInt();
        switch (choice) {
            case 1:
                System.out.println(zdanie.length());
                break;
            case 2:
                int licznik = 0;
                for (int i = 0; i < zdanie.length(); i++) {
                    if (zdanie.charAt(i) != ' ' && zdanie.charAt(i) != '\t') {
                        licznik++;
                    }
                }
                System.out.println(licznik);
                break;
            case 3:
                System.out.println("Żegnaj :)");
                break;
            default:
                System.out.println("Błędne dane");
                break;

        }


    }
}
