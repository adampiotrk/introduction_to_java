package zadania.CaesarCipher;

import java.util.Scanner;

// program sluzacy do szyfrowania i deszyfrowania metoda Cezara
public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice;
        String sentence;
        int shift;

        System.out.println("Witaj w programie sluzacym do szyfrowania i odszyfrowywania zdan metoda Cezera");

        do {
            System.out.println("Co chcesz zrobic?");
            System.out.println("1. Zaszyfrowac zdanie");
            System.out.println("2. Odszyfrowac zdanie");
            System.out.println("3. Zakonczyc program");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("Wpisz zdanie do zaszyfrowania");
                    sentence = scanner.nextLine();
                    System.out.println("Podaj krok szyfrowania");
                    shift = scanner.nextInt();
                    Cipher.encrypter(sentence, shift);
                    System.out.println();
                    System.out.println();
                    break;
                case 2:
                    System.out.println("Wpisz zdanie do odszyfrowania");
                    sentence = scanner.nextLine();
                    System.out.println("Podaj krok deszyfracji");
                    shift = scanner.nextInt();
                    Cipher.decrypter(sentence, shift);
                    System.out.println();
                    System.out.println();
                    break;
                case 3:
                    break;
                default:
                    break;

            }
        } while (choice != 3);
    }
}


