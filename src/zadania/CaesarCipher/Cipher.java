package zadania.CaesarCipher;

public class Cipher {

    /*
Metoda szyfrujaca zmienna typu String z przesunieciem o parametr oraz wyswietlajca zaszyfrowany tekst
 */
    public static void encrypter(String toEncrypt, int parameter) {
        char[] array = toEncrypt.toCharArray();

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 65 && array[i] <= 90) {
                if (array[i] + parameter > 90) {
                    array[i] = (char) (array[i] + parameter - 26);
                } else {
                    array[i] += parameter;
                }
            }
            if (array[i] >= 97 && array[i] <= 122) {
                if (array[i] + parameter > 122) {
                    array[i] = (char) (array[i] + parameter - 26);
                } else {
                    array[i] += parameter;
                }
            }
            System.out.print(array[i]);
        }
    }

    /*
Metoda deszyfrujaca zmienna typu String z przesunieciem o parametr oraz wyswietlajca odszyfrowany tekst
*/
    public static void decrypter(String toDecrypt, int parameter) {
        char[] array = toDecrypt.toCharArray();

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 65 && array[i] <= 90) {
                if (array[i] - parameter < 65) {
                    array[i] = (char) (array[i] - parameter + 26);
                } else {
                    array[i] -= parameter;
                }
            }
            if (array[i] >= 97 && array[i] <= 122) {
                if (array[i] - parameter < 97) {
                    array[i] = (char) (array[i] - parameter + 26);
                } else {
                    array[i] -= parameter;
                }
            }
            System.out.print(array[i]);
        }
    }
}
