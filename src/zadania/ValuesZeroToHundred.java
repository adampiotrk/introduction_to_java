package zadania;


// program wypisujacy liczbyy calkowite od 0 do 100
//z wykorzytaniem dwoch rodzajow petli
public class ValuesZeroToHundred {

    public static void main(String[] args) {
        int number;

        System.out.println("----- wyswietlanie za pomoca petli FOR ------");
        for (number=0; number <= 100; number++) {
            System.out.println(number);
        }


        System.out.println("------- wyswietlanie za pomoca petli WHILE -----");
        number = 0;
        while (number<=100) {
            System.out.println(number);
            number++;
        }
    }
}
