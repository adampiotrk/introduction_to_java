package zadania;

import java.util.Scanner;

public class SumFromZeroToHundred {

    public static void main(String[] args) {
        int x = 0;
        int bottom;
        int top;

        System.out.println("Application sumujacy liczby całkowite w zadanym przedziale");
        Scanner input = new Scanner(System.in);

        System.out.println("Podaj dolną wartość przedziału");
        bottom = input.nextInt();

        System.out.println("Podaj gorna wartosc przedzialu");
        top = input.nextInt();

        for (int i = bottom; i <= top; i++) {
            x = x + i;
        }
        System.out.println("Suma liczb calkowitych w tym przedziale = " + x);
    }
}
