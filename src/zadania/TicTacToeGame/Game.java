package zadania.TicTacToeGame;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w grze w kolko i krzyzyk");
        System.out.println("Wprowadz imie pierwszego gracza - gracz uzywajacy symbolu X");
        Player player1 = new Player(scanner.nextLine(), 'X');
        System.out.println("Wprowadz imie drugiego gracz - gracz uzywajacy symbolu O");
        Player player2 = new Player(scanner.nextLine(), 'O');
        System.out.println("Witajcie " + player1.getName() + " i " + player2.getName() + " czas rozpoczac gre");

        char[][] gameField = createEmptyField();
        int[] currentRowAndColumn;
        int round = 0;
        while (round <= 9) {
            currentRowAndColumn = getPlayerCoordinates(gameField, player1.getName(), player1.getSymbol());
            printGameField(gameField);
            round++;
            if (checkWinner(gameField, player1.getSymbol(), currentRowAndColumn[0], currentRowAndColumn[1])) {
                System.out.println(String.format("Gratulacje %s! Wygrales!", player1.getName()));
                break;
            }
            if (round == 9) {
                System.out.println("Remis!");
                break;
            }
            currentRowAndColumn = getPlayerCoordinates(gameField, player2.getName(), player2.getSymbol());
            printGameField(gameField);
            round++;
            if (checkWinner(gameField, player2.getSymbol(), currentRowAndColumn[0], currentRowAndColumn[1])) {
                System.out.println(String.format("Gratulacje %s! Wygrales!", player2.getName()));
                break;
            }
        }
    }

    public static boolean checkWinner(char[][] gameField, char symbol, int row, int column) {
        boolean isWinner = false;
        if ((gameField[row][0] == symbol && gameField[row][1] == symbol && gameField[row][2] == symbol)
                || (gameField[0][column] == symbol && gameField[1][column] == symbol && gameField[2][column] == symbol)
                || (gameField[0][0] == symbol && gameField[1][1] == symbol && gameField[2][2] == symbol)
                || (gameField[0][2] == symbol && gameField[1][1] == symbol && gameField[2][0] == symbol)
                ) {
            isWinner = true;
        }
        return isWinner;
    }

    private static int[] getPlayerCoordinates(char[][] gameField, String player, char symbol) {
        Scanner scanner = new Scanner(System.in);


        System.out.println(player + " podaj numer wiersza i kolumny");
        int[] coordinates = new int[2];
        coordinates[0] = scanner.nextInt() - 1;
        coordinates[1] = scanner.nextInt() - 1;

        while (gameField[coordinates[0]][coordinates[1]] != '-') {

            System.out.println("Pole jest juz zajete, wybierz ponownie");
            coordinates[0] = scanner.nextInt() - 1;
            coordinates[1] = scanner.nextInt() - 1;

        }

        gameField[coordinates[0]][coordinates[1]] = symbol;
        return coordinates;
    }

    private static void printGameField(char[][] gameField) {
        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField[i].length; j++) {
                System.out.print(gameField[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-----------");
        }
    }

    private static char[][] createEmptyField() {
        char[][] gameField = new char[3][3];
        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField[i].length; j++) {
                gameField[i][j] = '-';
            }
        }
        return gameField;
    }
}
