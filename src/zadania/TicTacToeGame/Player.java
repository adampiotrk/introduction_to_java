package zadania.TicTacToeGame;

public class Player {
    private String name;
    private char symbol;
    int wins = 0;
    int losses = 0;

    public String getName() {
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public Player(String name, char symbol) {
        this.name = name;
        this.symbol = symbol;
    }
}
