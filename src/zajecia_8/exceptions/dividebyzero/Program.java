package zajecia_8.exceptions.dividebyzero;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.println("Podaj pierwsza liczbe ");
            int liczba = scanner.nextInt();
            System.out.println("Podaj druga liczbe");
            int drugaLiczba = scanner.nextInt();
            System.out.println("Jestem w try");
            int result = liczba / drugaLiczba;
            System.out.println("po dzieleniu");
            System.out.println("Wynik: " + result);
            int[] arr = new int[2];
            int wynik = arr[5];
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
            System.out.println("Nie mozna dzielic przez 0!");
        } catch (InputMismatchException e) {
            System.out.println("Nie podano liczby");
        } catch (Exception ex) {
            System.out.println("Wystapil blad: " + ex.getMessage());
        }
    }
}
