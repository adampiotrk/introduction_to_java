package zajecia_6.zadanie2;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Point point1 = new Point();

        Point newpoint = new Point(2,5);

        System.out.println("Podaj wspolrzedna x");
        point1.setX(scanner.nextDouble());
        System.out.println("Podaj wspolrzedna y");
        point1.setY(scanner.nextDouble());

        System.out.println("Odleglosc od poczatku ukladu wspolrzednych = " + point1.distanceFromOrigin());


    }
}
