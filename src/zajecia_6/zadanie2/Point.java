package zajecia_6.zadanie2;

public class Point {
    private double x;
    private double y;

    // domyslny konstruktor, tworzy sie automatycznie przy tworzeniu klasy
    // bez voida i parametrow
    // konstruktor bezparametrowy
    public Point() {
    }

    // przeciazony kontruktor, z inna sygnatura
    // o takiej samej nazwie, ale z parametrami w nawiasie
    // jezeli napiszemy konstruktor przeciazony, to automatyczne generowanie pustego konstruktora nie nastepuje
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distanceFromOrigin() {
        return Math.sqrt((x * x) + (y * y));
    }
}
