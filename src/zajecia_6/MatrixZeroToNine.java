package zajecia_6;

public class MatrixZeroToNine {
    public static void main(String[] args) {

        int[][] matrix = MatrixHelper.createMatrix();

        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == j) {
                    matrix[i][j] = i;
                    sum = sum + matrix[i][j];
                } else {
                    matrix[i][j] = 0;
                }
            }
        }
        MatrixHelper.printMatrix(matrix);
        System.out.println("Suma wartosci na przekatnej macierzy = " + sum);
    }
}
