package zajecia_6.zadanie4;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w programie :)");
        System.out.println("Podaj nazwe firmy:");
        String name = scanner.nextLine();
        Company myCompany = new Company(name);

        printMenu();
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                add(myCompany);
                print(myCompany);
                break;
            case 2:
                print(myCompany);
                break;
            default:
                System.out.println("Nieprawidlowy wybor");
                break;
        }
    }

    private static void print(Company myCompany) {
        for (Employee e : myCompany.getEmployees()) {
            if (e != null) {
                System.out.println(e.getDescription());
            }
        }
    }

    private static void add(Company myCompany) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj imie: ");
        String empName = scanner.nextLine();
        System.out.println("Podaj nazwisko: ");
        String empSurname = scanner.nextLine();
        System.out.println("Podaj pensje: ");
        double empSalary = scanner.nextDouble();

        Employee employee = new Employee(empName, empSurname, empSalary);
        boolean isSuccess = myCompany.addEmployee(employee);
        if (isSuccess) {
            System.out.println("Dodano pracownika");
        } else {
            System.out.println("Nie udalo sie dodac - za duzo pracownikow");
        }
    }

    private static void printMenu() {
        System.out.println("Wybierz opcje: ");
        System.out.println("1. Dodawanie pracownika");
        System.out.println("2. Wyswietlanie pracownikow");
        System.out.println("Wybór: ");
    }
}
