package zajecia_6.zadanie4;

public class Employee {
    private String name;
    private String surname;
    private String email;
    private int age;
    private double salary;

    public Employee(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Employee(String name, String surname, int age) {
        this(name, surname);
        this.age = age;
    }

    public Employee(String name, String surname, double salary) {
        this(name, surname);
        this.salary = salary;
    }

    public Employee(String name, String surname, int age, double salary) {
        this(name, surname, age);
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDescription() {
        return String.format("Name: %s, Surname: %s, Salary: %.2f", name, surname, salary);
    }


}
