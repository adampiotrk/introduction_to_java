package zajecia_6.zadanie4;

public class Company {
    private static final int DEFAULT_SIZE = 50;
    private String name;
    private Employee[] employees;
    private int companySize = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee[] getEmployees() {
        return employees;
    }

    public Company(String name) {
        this.name = name;
        this.employees = new Employee[DEFAULT_SIZE];
    }

    public Company(String name, int initialSize) {
        this.name = name;
        this.employees = new Employee[initialSize];
    }

    public boolean addEmployee(Employee emp) {
        if (companySize < employees.length) {
            employees[companySize++] = emp;
            return true;
        }
        return false;
    }


}
