package zajecia_6;


/**
 * Klasa Person do reprezentacji danych o osobie
 */
public class Person {
    // stan - mowi o tym jaka osoba jest/ przechowuje jej dane
    // nazywamy to polem klasy
    public String name;
    public String surname;
    public int age;
    public String email;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Person(String name, String surname,  int age) {
        this(name, surname);
        this.age=age;
    }

    public Person(String name, String surname, int age, String email) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.email = email;
    }
    // zachowanie - jakas czynnosc

    public void sayHello() {
        System.out.println("Jestem: " + name + " " + surname);
    }

}
