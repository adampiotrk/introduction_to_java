package zajecia_6;

import java.util.Random;
import java.util.Scanner;

public class MatrixHelper {

    public static void printMatrix(int[][] m) {
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static int[][] createMatrix() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wielkosc tablicy liczba wierszy x liczba kolumn: ");
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int[][] matrix = new int[m][n];
        return matrix;
    }

    public static void fillMatrix(int[][] matrix) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Co chcesz zrobic?");
        System.out.println("1. Wpisz wartosci tablicy");
        System.out.println("2. Wylosuj wartosci tablicy");
        int choice = scanner.nextInt();

        switch (choice) {
            case 1:
                System.out.println("Podaj wartosci tablicy ");
                for (int i = 0; i < matrix.length; i++) {
                    for (int j = 0; j < matrix[i].length; j++) {
                        matrix[i][j] = scanner.nextInt();
                    }
                }
                break;
            case 2:
                System.out.println("Podaj granice przedzialu, z ktorego beda losowane liczby");
                int bottom = scanner.nextInt();
                int top = scanner.nextInt();
                for (int i = 0; i < matrix.length; i++) {
                    for (int j = 0; j < matrix[i].length; j++) {
                        matrix[i][j] = random.nextInt(top - bottom) + bottom;
                    }
                }
                break;
            default:
                System.out.println("Nieprawidlowa wartosc");
                break;
        }
    }

    public static int[][] addMatrix(int[][] a, int[][] b) {
        if (!validate(a, b)) {
            return null;
        }

        int[][] result = new int[a.length][a[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
        }
        return result;
    }

    private static boolean validate(int[][] a, int[][] b) {
        if (a.length != b.length) {
            return false;
        }

        for (int i = 0; i < a.length; i++) {
            if (a[i].length != b[i].length) {
                return false;
            }
        }
        return true;
    }


}
