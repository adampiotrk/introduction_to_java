package zajecia_6;


/// OOP - object oriented programming
public class OopIntro {
    public static void main(String[] args) {
        Person me = new Person("Adam", "Kowalski");
        me.name = "Adam";
        me.surname = "Kowalski";
        me.age = 25;
        me.email = "mail@op.pl";

        Person student = new Person("Jan", "Nowak", 22);
        student.email = "jan@wp.pl";

        me.sayHello();
        student.sayHello();

    }

}
