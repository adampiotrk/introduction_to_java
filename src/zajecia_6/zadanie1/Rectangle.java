package zajecia_6.zadanie1;


/**
 * Klasa do reprezentacji prostokata
 */
public class Rectangle {
    // dane opisujace prostokat
    private double a;
    private double b;

    // getter i setter dla pola a
    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public void setA(double a) {
        if (a <= 0) {
            throw new IllegalArgumentException("Dlugosc mniejsza od 0");
        }
        this.a = a;
    }

    public void setB(double b) {
        if (a <= 0) {
            throw new IllegalArgumentException("Dlugosc mniejsza od 0");
        }
        this.b = b;
    }

    // metoda obliczajaca pole
    public double calculateArea() {
        return a * b;
    }

    //metoda obliczajaca obwod
    public double calculatePerimeter() {
        return 2 * a + 2 * b;
    }
}
