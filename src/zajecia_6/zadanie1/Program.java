package zajecia_6.zadanie1;

public class Program {
    public static void main(String[] args) {
        // stworzyc 3 obiekty typu prostokat i wywolac jego metody
        Rectangle rec1 = new Rectangle();
//        rec1.a = 2;
        rec1.setA(2);
//        rec1.b = 3;
        rec1.setB(3);

        Rectangle rec2 = new Rectangle();
//        rec2.a = 5;
//        rec2.b = 9;
        rec2.setA(5);
        rec2.setB(9);

        Rectangle rec3 = new Rectangle();
//        rec3.a = 8;
//        rec3.b = 12;
        rec3.setA(8);
        rec3.setB(12);

        System.out.println("Prostokat ma boki: " + rec1.getA() + " i " + rec1.getB() + ". Pole = " + rec1.calculateArea() + ". Obwod = " + rec1.calculatePerimeter());
        System.out.println("Prostokat ma boki: " + rec2.getA() + " i " + rec2.getB() + ". Pole = " + rec2.calculateArea() + ". Obwod = " + rec2.calculatePerimeter());
        System.out.println("Prostokat ma boki: " + rec3.getA() + " i " + rec3.getB() + ". Pole = " + rec3.calculateArea() + ". Obwod = " + rec3.calculatePerimeter());
    }
}
