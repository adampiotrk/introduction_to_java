package zajecia_5;

import java.util.Random;
import java.util.Scanner;

public class CountMaxsInArray {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Podaj wielkosc tablicy");
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
        }

        int max = ArrayHelper.maxInArray(array);

        int howMany = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == max) {
                howMany++;
            }
        }

        System.out.println("Najwieksza liczba w zbiorze = " + max + ". Wystepuje ona " + howMany + " razy.");
    }
}
