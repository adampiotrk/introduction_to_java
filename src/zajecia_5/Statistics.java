package zajecia_5;

import java.util.Scanner;

public class Statistics {

    public static double averageInArray(double[] array) {
        double sum = sumOfArrayElements(array);
        double average = sum / array.length;
        return average;
    }

    public static double sumOfArrayElements(double[] array) {
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        return sum;
    }

    public static double maxInArray(double[] array) {
        double max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }

    public static double minInArray(double[] array) {
        double min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }

    public static void printMenu() {
        System.out.println("1. Srednia z tych liczb");
        System.out.println("2. Suma tych liczb");
        System.out.println("3. Wartosc max");
        System.out.println("4. Wartosc min");
        System.out.println("Twoj wybor: ");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile liczb bedzie wprowadzonych?");
        int n = scanner.nextInt();
        double[] array = new double[n];

        System.out.println("Wprowadz kolejno liczby do obliczen:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextDouble();
        }

        printMenu();
        int choice = scanner.nextInt();
        double result;
        switch (choice) {
            case 1:
                result = averageInArray(array);
                System.out.println(result);
                break;
            case 2:
                result = sumOfArrayElements(array);
                System.out.println(result);
                break;
            case 3:
                result = maxInArray(array);
                System.out.println(result);
                break;
            case 4:
                result = minInArray(array);
                System.out.println(result);
                break;
            default:
                System.out.println("Bledny wybor");

        }
    }
}
