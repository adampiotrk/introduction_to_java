package zajecia_5;

public class ArrayHelper {

    /**
     * Metoda sprawdzajaca w tablicy integerow czy podany element wystepuje w tablicy
     */
    public static boolean isPresent(int[] array, int element) {
        boolean present = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element) {
                present = true;
                break;
            }
        }
        return present;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static int maxInArray(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }

    public static int minInArray(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }
}
