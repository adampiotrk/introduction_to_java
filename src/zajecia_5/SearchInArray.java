package zajecia_5;

import java.util.Random;
import java.util.Scanner;

public class SearchInArray {

    public static void main(String[] args) {
        int[] tab = new int[30];
        for (int i = 0; i < tab.length; i++) {
            Random random = new Random();
            tab[i] = random.nextInt(100);
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe z przedzialu od 0 do 100");
        int number = scanner.nextInt();

        // ten fragment zostal przeniesiony do metody ArrayHelper.isPresent
//        boolean isInArray = false;
//        for (int i = 0; i < tab.length; i++) {
//            if (number == tab[i]) {
//                isInArray = true;
//                break;
//            }
//        }
        boolean isInArray = ArrayHelper.isPresent(tab, number);

        System.out.println(isInArray ? "Element wystepuje w tablicy" : "Element nie wystepuje w tablicy");

    }
}
