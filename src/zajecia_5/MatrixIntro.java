package zajecia_5;

import zajecia_6.MatrixHelper;

public class MatrixIntro {

    public static void main(String[] args) {
        int[][] matrix = MatrixHelper.createMatrix();
        MatrixHelper.fillMatrix(matrix);

        int[][] secondMatrix = MatrixHelper.createMatrix();
        MatrixHelper.fillMatrix(secondMatrix);

        System.out.println("Macierz pierwsza =");
        MatrixHelper.printMatrix(matrix);
        System.out.println("Macierz druga =");
        MatrixHelper.printMatrix(secondMatrix);

        int[][] sumMatrix = MatrixHelper.addMatrix(matrix, secondMatrix);
        if (sumMatrix == null) {
            System.out.println("Nie mozna dodac macierzy");
        } else {
            System.out.println("Suma macierzy =");
            MatrixHelper.printMatrix(sumMatrix);
        }
    }
}
