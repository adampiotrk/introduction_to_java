package zajecia_5;

import java.util.Scanner;

public class SearchStringInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // utworzyc tablice 5 napisow i od razu ja zainicjalizowac
        String[] surnames = {"Kowalski", "Nowak", "Kuciak", "Ligas", "Kolano"};

        // zapytac o nazwisko
        System.out.println("Wpisz nazwisko do sprawdzenia obecnosci w tablicy");
        String surname = scanner.nextLine();

        //odpowiedziec czy wystepuje
        boolean isPresent = false;
        for (int i = 0; i < surnames.length; i++) {
            if (surnames[i].equals(surname)) {
                isPresent = true;
                break;
            }
        }
        System.out.println(isPresent ? surname + " znajduje sie w tablicy" : surname + " nie znajduje sie w tablicy");
    }
}
