package zajecia_5;

import zajecia_4.MathHelper;

import java.util.Random;
import java.util.Scanner;

public class ScalarProduct {

    // deklaracja stalej, zmiennej finalnej, w konwencji duzymi literami
    public static final int ARRAY_SIZE = 50;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Application oblicza iloczyn skalarny dwoch wektorow. Podaj n-wymiar wektorow:");
        int n = scanner.nextInt();
        int[] vectorA = new int[n];
        int[] vectorB = new int[n];

        for (int i = 0; i < n; i++) {
            vectorA[i] = random.nextInt(30) + 1;
            vectorB[i] = random.nextInt(30) + 1;
        }
        System.out.print("Wektor A: ");
        ArrayHelper.printArray(vectorA);
        System.out.print("Wektor B: ");
        ArrayHelper.printArray(vectorB);

        int scalar = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("Iloczy skalarny dla n" + (i+1) + " = " + scalar);
            scalar = scalar + (vectorA[i] * vectorB[i]);
            System.out.print(" + (" + vectorA[i] + " * " + vectorB[i] + ") = " + scalar);
            System.out.println();
        }
    }
}
