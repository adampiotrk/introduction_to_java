package zajecia_5;

import java.util.Scanner;

public class StringConverter {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadz napis do 20 znakow");
        String napis = scanner.nextLine();

        if (napis.length() < 20) {
            printMenu();
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    toUpper(napis);
                    break;
                case 2:
                    toLower(napis);
                    break;
                case 3:
                    toggleCase(napis);
                    break;
            }
        } else {
            System.out.println("Zbyt dlugie wyrazenie");
        }
    }

    public static void printMenu() {
        System.out.println("1. Wszystkie litery na duze");
        System.out.println("2. Wszystkie litery na male");
        System.out.println("3. Wszystkie male litery na duze i duze na male");
        System.out.println("Twoj wybor: ");
    }

    public static void toLower(String napis) {
        char[] array = napis.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if ((byte) array[i] >= 65 && (byte) array[i] <= 90) {
                array[i] += 32;
            }
            System.out.print(array[i]);
        }
    }

    public static void toUpper(String napis) {
        char[] array = napis.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if ((byte) array[i] >= 97 && (byte) array[i] <= 122) {
                array[i] -= 32;
            }
            System.out.print(array[i]);
        }
    }

    public static void toggleCase(String napis) {
        char[] array = napis.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if ((byte) array[i] >= 65 && (byte) array[i] <= 90) {
                array[i] += 32;
            } else if ((byte) array[i] >= 97 && (byte) array[i] <= 122) {
                array[i] -= 32;
            }
            System.out.print(array[i]);
        }
    }

}

