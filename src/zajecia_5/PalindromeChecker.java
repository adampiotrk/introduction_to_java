package zajecia_5;

import java.util.Scanner;

// przejrzec sobie wersje na BitBuckecie od Piotra
// inna metoda zrobione, poprzez dzielenie slowa na pol
public class PalindromeChecker {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Application sprawdzi czy zdanie jest palindromem. Wpisz zdanie:");
        String napis = scanner.nextLine();
        napis = napis.toLowerCase().replace(" ", "");

        char[] array = napis.toCharArray();
        char[] arrayReversed = new char[array.length];

        int lenght = array.length - 1;
        for (int i = 0; i < array.length; i++) {
            arrayReversed[lenght] = array[i];
            lenght--;
        }

        for (int i = 0; i < arrayReversed.length; i++) {
            System.out.print(arrayReversed[i]);
        }
        System.out.println();

        boolean isPalindrome = true;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != arrayReversed[i]) {
                isPalindrome = false;
            }
        }

        if (napis.length() % 2 == 0 && isPalindrome == true) {
            System.out.println("Palindrom parzysty");
        } else if (napis.length() % 2 != 0 && isPalindrome == true) {
            System.out.println("Palindrom nieparzysty");
        } else {
            System.out.println("Nie jest palindromem");
        }
    }
}
