package zajecia_7.abstract_figures;

public class Circle extends Figure {
    final double pi = 3.14;
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        return pi * radius * radius;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * pi * radius;
    }
}
