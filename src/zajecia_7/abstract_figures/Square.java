package zajecia_7.abstract_figures;

public class Square extends Figure {
    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double calculateArea() {
        return 2 * a;
    }

    @Override
    public double calculatePerimeter() {
        return 4 * a;
    }
}
