package zajecia_7.abstract_figures;

public abstract class Figure {

    public abstract double calculateArea();

    public abstract double calculatePerimeter();

}
