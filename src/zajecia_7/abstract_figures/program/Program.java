package zajecia_7.abstract_figures.program;

import zajecia_7.abstract_figures.Circle;
import zajecia_7.abstract_figures.Figure;
import zajecia_7.abstract_figures.Rectangle;
import zajecia_7.abstract_figures.Square;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Figure[] figureArray = new Figure[10];
        int amount = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("Dodaj nowa figure");
            System.out.println("1. Kwadrat");
            System.out.println("2. Prostokat");
            System.out.println("3. Kolo");
            System.out.println("4. Pola wszystkich figur");
            System.out.println("5. Obwody wszystkich figur");
            System.out.println("6. Wyjsc");
            int choice = scanner.nextInt();
            Figure figure;
            switch (choice) {
                case 1:
                    System.out.println("Podaj bok kwadratu");
                    double a1 = scanner.nextDouble();
                    figure = new Square(a1);
                    figureArray[amount++] = figure;
                    break;
                case 2:
                    System.out.println("Podaj boki prosokata");
                    double a2 = scanner.nextDouble();
                    double b2 = scanner.nextDouble();
                    figure = new Rectangle(a2, b2);
                    figureArray[amount++] = figure;
                    break;
                case 3:
                    System.out.println("Podaj promien okregu");
                    double radius = scanner.nextDouble();
                    figure = new Circle(radius);
                    figureArray[amount++] = figure;
                    break;
                case 4:
                    System.out.println("Pola wszystkich figur:");
                    for (int i = 0; i < amount; i++) {
                        Figure f = figureArray[i];
                        System.out.println(f.calculateArea());
                        System.out.println();
                    }
                    break;
                case 5:
                    System.out.println("Obwody wszystkich figur:");
                    for (int i = 0; i < amount; i++) {
                        Figure f = figureArray[i];
                        System.out.println(f.calculatePerimeter());
                        System.out.println();
                    }
                    break;
                case 6:
                    exit = true;
                    break;
                default:
                    System.out.println("Nieprawidlowy wybor");
                    break;

            }
        }
    }
}
