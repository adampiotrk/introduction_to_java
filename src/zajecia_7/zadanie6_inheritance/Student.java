package zajecia_7.zadanie6_inheritance;

// klasa pochodna / rozszerzajaca - sub class
public class Student extends Person {
    private String university;
    private String studies;
    private int studentID;

    public Student(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Student(String name, String surname, int age, int studentID, String university) {
        super(name, surname, age);
        this.studentID = studentID;
        this.university = university;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getStudies() {
        return studies;
    }

    public void setStudies(String studies) {
        this.studies = studies;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    @Override
    public String describe() {
        String originalDescription = super.describe();
        return String.format("%s, uczelnia: %s, numer indeksu: %d", originalDescription, getUniversity(), getStudentID());
    }

    // mozemy przeladowac / nadpisac metody, ktore sa w klasie Object, zastepujemy domyslne toString naszym formatem wyswietlania
    @Override
    public String toString() {
        return String.format("%s %s, uczelnia: %s, numer indeksu: %d", getName(), getSurname(), getUniversity(), getStudentID());
    }
}
