package zajecia_7.zadanie6_inheritance;

public class InheritanceIntro {
    public static void main(String[] args) {
        Person ziomek = new Person("Stefan", "Smietana", 25);
        System.out.println(ziomek.describe());
        Student student = new Student("Franek", "Kimono", 22);
        System.out.println(student.describe());

        Student studentInformatyki = new Student("Maria", "Jopek", 21, 10568, "UAM");
        System.out.println(studentInformatyki.describe());

        Worker worker = new Worker("Jan", "Pawlak", 34, 8500.65, "Glowny projektant");
        System.out.println(worker);
    }
}
