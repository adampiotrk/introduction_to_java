package zajecia_7.zadanie5_matrix;

import java.util.Random;

public class Matrix {
    private int columns;
    private int rows;
    private int[][] matrix;

    public Matrix(int rows, int columns) {
        this.columns = columns;
        this.rows = rows;
        matrix = new int[rows][columns];
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    /**
     * Adds second matrix given as a parameter
     */
    public Matrix addMatrix(Matrix second) {
        // najpierw sprawdzic czy wymiary drugiej sie zgadzaja


        // przejsc do obliczen
        Matrix result = new Matrix(this.rows, this.columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result.matrix[i][j] = this.matrix[i][j] + second.matrix[i][j];
            }
        }
        return result;
    }

    public void print() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(matrix[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Fills matrix with random values with bound set as parameter
     */
    public void fillWithRandomValues(int bound) {
        Random random = new Random();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix[i][j] = random.nextInt(bound);
            }
        }
    }

}
