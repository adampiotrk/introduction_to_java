package zajecia_7.zadanie5_matrix;

public class Application {
    public static void main(String[] args) {

        Matrix first = new Matrix(4, 3);
        first.fillWithRandomValues(50);
        first.print();

        Matrix second = new Matrix(4, 3);
        second.fillWithRandomValues(80);
        second.print();

        Matrix result = first.addMatrix(second);
        result.print();

    }
}
