package zajecia_2.odczyt_z_konsoli;

import java.util.Scanner;

// program pobierajacy dwie liczby calkowite i obliczajacy wyrazenie logiczne
// a>=b && a!=b

public class OperatoryLogiczneIntro {

    public static void main(String[] args) {
        int a;
        int b;
        boolean wynik;

        Scanner odczyt = new Scanner(System.in);

        System.out.println("Podaj liczbę a");
        a = odczyt.nextInt();

        System.out.println("Podaj liczbę b");
        b = odczyt.nextInt();

        wynik = (a >= b) && (a != b);

        System.out.println("Czy a jest większe i różne od b?");
        System.out.println(wynik);

    }

}

