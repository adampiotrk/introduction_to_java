package zajecia_2.odczyt_z_konsoli;

import java.util.Scanner;
import java.util.SortedMap;

//program pobierajacy imie, nawisko i wiek z konsoli i drukujacy dane osoby

public class ScannerIntro {

    public static void main(String[] args) {
        //zapytaj o imie
        String name;
        String surname;
        int age;

        System.out.println("Jak masz na imię?");

        // utworzenie obiektu typu scanner o nazwie odczyt
        // obiekty zawsze wywoluje sie z poleceniem "new"
        Scanner odczyt = new Scanner(System.in);

        // do zmiennej imie wstaw nastepna linie z konsoli
        name = odczyt.nextLine();

        //odczytaj z konsoli linie tekstu i wstaw do zmiennej nazwisko
        System.out.println("Jak masz na nazwisko?");
        surname = odczyt.nextLine();

        //odczytaj z konsoli liczbe i wstaw do zmiennej age
        System.out.println("Ile masz lat?");
        age = odczyt.nextInt();

        System.out.println("Witaj, " + name + " " + surname + "!");
        System.out.println("Masz " + age + " lat ;)");

    }

}

