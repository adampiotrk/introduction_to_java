package zajecia_2.instrukcjesterujace;

import java.util.Scanner;

/* dla podanych dwoch licz z klawiatury szukamy wiekszej
zakladamy, ze podajemy rozne liczby
 */
public class FindGreaterNumber {

    public static void main (String [] args) {
        int a;
        int b;

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj dwie liczby calkowite");
        a = input.nextInt();
        b = input.nextInt();

        if (a>b) {
            System.out.println(a + " > " + b);
        } else if (a < b){
            System.out.println(a + " < " + b);
        } else {
            System.out.println(a + " = " + b);
        }
    }
}
