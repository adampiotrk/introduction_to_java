package zajecia_2.instrukcjesterujace;

import java.util.Scanner;

public class CalculatorBMI {

    public static void main (String [] args) {
        float height;
        float weight;
        float bmi;

        Scanner input = new Scanner(System.in);

        System.out.println("Witaj, podaj swój wzrost w m");
        height = input.nextFloat();

        System.out.println("Podaj swoją wagę w kg");
        weight = input.nextFloat();

        bmi = weight / (height*height);

        if (bmi < 18.5) {
            System.out.println("Niedowaga");
        } else if (bmi <= 24.9) {
            System.out.println("Waga prawidłowa");
        } else if (bmi <29) {
            System.out.println("Nadwaga");
        } else {
            System.out.println("Otyłość I stopnia");
        }

    }
}
