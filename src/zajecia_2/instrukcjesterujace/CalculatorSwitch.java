package zajecia_2.instrukcjesterujace;

import java.util.Scanner;

public class CalculatorSwitch {

    public static void main(String[] args) {

        double a;
        double b;
        int choice;

        Scanner input = new Scanner(System.in);
        System.out.println("Kalkulator dla dwoch dowolnych liczb");
        System.out.println("Podaj wartość a");
        a = input.nextDouble();
        System.out.println("Podaj wartość b");
        b = input.nextDouble();

        System.out.println("\n------- Co chcesz zrobić? --------");
        System.out.println("1. Dodawanie");
        System.out.println("2. Odejmowanie");
        System.out.println("3. Mnożenie");
        System.out.println("4. Dzielenie");
        System.out.println("5. Reszta z dzielenia");
        choice = input.nextInt();

        switch (choice) {
            case 1:
                System.out.println(a + b);
                break;
            case 2:
                System.out.println(a - b);
                break;
            case 3:
                System.out.println(a * b);
                break;
            case 4:
                System.out.println(a / b);
                break;
            case 5:
                System.out.println(a % b);
                break;
            default:
                System.out.println("Nieprawidłowy wybór");
                break;
        }
    }
}
