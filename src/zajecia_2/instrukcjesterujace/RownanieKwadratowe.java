package zajecia_2.instrukcjesterujace;

import java.util.Scanner;

public class RownanieKwadratowe {

    public static void main(String[] args) {

        double a;
        double b;
        double c;
        double delta;
        double x1;
        double x2;

        Scanner input = new Scanner(System.in);

        System.out.println("Application do obliczania pierwiastkow rowniania kwadratowego = ax^2 + bx + c");
        System.out.println("Podaj wartosc a");
        a = input.nextDouble();
        System.out.println("Podaj wartosc b");
        b = input.nextDouble();
        System.out.println("Podaj wartosc c");
        c = input.nextDouble();

        delta = b * b - (4 * a * c);

/////////rownianie kwadratowe z zastosowaniem if else

//        if (delta == 0) {
//            x1 = -b / (2 * a);
//            System.out.println("Jedno miejsce zerowe = " + x1);
//        } else if (delta < 0) {
//            System.out.println("Brak miejsca zerowego");
//        } else {
//            x1 = (-b - Math.sqrt(delta)) / (2 * a);
//            x2 = (-b + Math.sqrt(delta)) / (2 * a);
//            System.out.println("x1 = " + x1);
//            System.out.println("x2 = " + x2);
//        }

//// rownanie kwadratowe z zastosowaniem switcha
        int iloscRozwiazan;

        if (delta == 0) {
            iloscRozwiazan=1;
        } else if (delta < 0) {
            iloscRozwiazan=0;
        } else {
            iloscRozwiazan = 2;
        }
        switch (iloscRozwiazan) {
            case 0:
                System.out.println("Brak miejsca zerowego");
                break;
            case 1:
                x1 = -b / (2 * a);
                System.out.println("Jedno miejsce zerowe = " + x1);
                break;
            case 2:
                x1 = (-b - Math.sqrt(delta)) / (2 * a);
                x2 = (-b + Math.sqrt(delta)) / (2 * a);
                System.out.println("x1 = " + x1);
                System.out.println("x2 = " + x2);
                break;
            default:
                System.out.println("Nieprzewidziana sytuacja");
                break;
        }
    }
}
