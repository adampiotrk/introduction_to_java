package zajecia_2.instrukcjesterujace;

//// napisac petle ktora wypisze liczby od 0 do 10

public class WhileLoop {

    public static void main(String[] args) {

        int licznik = 0;

        while (licznik <= 10) {
            System.out.println(licznik);
            licznik++;
        }

    }

}
