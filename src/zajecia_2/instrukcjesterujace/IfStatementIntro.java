package zajecia_2.instrukcjesterujace;

import java.util.Scanner;

public class IfStatementIntro {
    public static void main (String [] args) {

        int wiek;

        Scanner input = new Scanner(System.in);

        System.out.println("Podaj wiek");
        wiek = input.nextInt();

        if (wiek >= 18) {
            System.out.println("Jesteś pełnoletni, masz dostęp do serwisu.");
        } else {
            System.out.println("Niestety, nie masz jeszcze 18 lat :(");
        }

    }
}
