package zajecia_2.konwerter;

import java.util.Scanner;

public class FahrenheitConverter {

    public static void main (String [] args) {
        double degreesC;
        double degreesF;

        Scanner input = new Scanner(System.in);

        System.out.println("Konwerter zmieniający stopnie Celsjusza na Fahrenheita. \nPodaj stopnie Celsjusza.");
        degreesC = input.nextDouble();

        degreesF = 1.8 * degreesC +32;

        System.out.println(degreesC + " stopnii Celsjusza = " + String.format("%.2f",degreesF) + " stopnii Fahrenheita");



    }

}
