package companyManager.fileoperation.reader;

import companyManager.Company;
import companyManager.Employee;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XmlEmployeeReader extends AbstractEmployeeReader {
    public XmlEmployeeReader(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public Employee[] readEmployees() {
        try {
            JAXBContext context = JAXBContext.newInstance(Company.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object object = unmarshaller.unmarshal(new File(pathToFile));
            Company result = (Company) object;
            return result.getEmployees();
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }
}
