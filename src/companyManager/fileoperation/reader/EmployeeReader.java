package companyManager.fileoperation.reader;
import companyManager.Employee;

/**
 * Wszystkie klasy ktore implementuja ten interfejs musza nadpisywac funkcje wczytaj pracownikow
 */
public interface EmployeeReader {
    Employee[] readEmployees();
}
