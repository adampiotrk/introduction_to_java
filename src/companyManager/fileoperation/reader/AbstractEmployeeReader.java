package companyManager.fileoperation.reader;

import companyManager.Employee;

/**
 * Abstrakcyjny czytnik - jedyne co wiem to sciezka do pliku, z ktorego chcemy czytac
 * Przechowujemy tutaj sciezke do pliku
 */

public abstract class AbstractEmployeeReader implements EmployeeReader {

    protected String pathToFile;

    //robimy konstruktor protected, zeby byl widoczny w klasach rozszerzajacych, ale zeby nie mozna bylo utworzyc obiektu tego typu
    protected AbstractEmployeeReader(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    @Override
    public abstract Employee[] readEmployees();
}
