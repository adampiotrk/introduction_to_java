package companyManager.fileoperation.writer;

import companyManager.Company;
import companyManager.Employee;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class XmlEmployeeWriter extends AbstractEmployeeWriter {
    public XmlEmployeeWriter(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public void writeEmployees(Employee[] employees) {
        Company companyToSave = new Company("");
        companyToSave.setEmployees(employees);
        try {
            JAXBContext context = JAXBContext.newInstance(Company.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(companyToSave, new File(pathToFile));
        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }
}
