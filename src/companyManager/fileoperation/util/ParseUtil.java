package companyManager.fileoperation.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Klasa narzedziowa pomagajaca w konwersji
 * Konwertuje double ze String
 * Pomocna, gdy liczba z przecinkiem jak w Polsce czy Francji
 */
public class ParseUtil {
    public static double parseDouble(String s) {
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        Number parse = null;
        try {
            parse = format.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parse.doubleValue();
    }
}
