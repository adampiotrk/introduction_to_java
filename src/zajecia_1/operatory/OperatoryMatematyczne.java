package zajecia_1.operatory;

    // Przyklad uzycia operatorow matematycznych dla 2 stalych liczb

public class OperatoryMatematyczne {

    public static void main (String[] args) {

        System.out.println("\n--------dzialania matematyczne-------");
        int a=16;
        int b=3;
        System.out.println("Obliczenia dla liczb a = " +a +" i b = " +b);
        int dodawanie=a+b;
        int odejmowanie=a-b;
        int mnozenie=a*b;
        int dzielenie=a/b;
        int reszta=a%b;
        System.out.println("Wynik dodawania = " +dodawanie);
        System.out.println("Wynik odejmowania = " +odejmowanie);
        System.out.println("Wynik mnozenia = " +mnozenie);
        System.out.println("Wynik dzielenia = " +dzielenie);
        System.out.println("Reszta z dzielenia = " +reszta);

        //dzielenie z reszta
        System.out.println("\n----Dzielenie z reszta----");
        System.out.println("Dzielenie z reszta = " + (float) a/b);

    }
}
