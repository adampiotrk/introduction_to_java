package zajecia_1.operatory;

//przyklad obliczania pola prostokata dla danych wartosci

import java.util.Scanner;

public class PoleProstokata {

    public static void main(String[] args) {

        //zamiana kodu na wczytywanie ze skanera
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj boki prostokata");
        double a = sc.nextDouble();
        double b = sc.nextDouble();

        //policz pole i wypisz
        double area = a * b;
        System.out.println("Pole prostokata wynosi " + area + " m2");

        //policz obwod i wypisz
        double obwod = 2 * a + 2 * b;
        System.out.println("Obwod prostokata wynosi " + obwod + " m");

        // czy pole jest wieksze od obwodu
        if (area == obwod) {
            System.out.println("Pole i obwod sa rowne");
        }
        if (area > obwod) {
            System.out.println("Pole jest wieksze od obwodu");
        } else {
            System.out.println("Pole jest mniejsze od obwodu");


        }
    }
}