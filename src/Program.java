/*pierwszy program,
piszemy sobie powitanie
do swiata
 */

//klasa - program
public class Program {
    //main - metoda o nazwie main
    public static void main(String[] args) {
        System.out.println("Hello World!"); /// \n tworzy nowa linie, jest wiecej znakow specjalnych: \" \t itd
        System.out.println("\nAdam");
        System.out.println("Marek");
        System.out.println("Basia");

/*        typy calkowitoliczbowe byte,short,int,long
        int wiek; deklaracja
        int wzrost=180; deklaracja i inicjalizacja
        */
        byte cyfra1 = 4;
        short cyfra2 = 7;
        int cyfra3 = 8864;
        System.out.println(cyfra1);
        System.out.println(cyfra2);
        System.out.println(cyfra3);

        //typy zmiennoprzecinkowe float, double
        float cyfra4 = 7.342F;
        double cyfra5 = 12.854885;
        System.out.println(cyfra4);
        System.out.println(cyfra5);

        //pojedyczny znak - character
        char character1 = 'B';
        System.out.println(character1);

        //lancuch znakow
        String name;        //deklaracja
        name = "Barbara";     //inicjalizacja
        System.out.println(name);

        //wartosc prawda/falsz
        boolean logic1 = true;
        System.out.println(logic1);


    }
}
